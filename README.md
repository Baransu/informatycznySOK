# InformatycznySOK
![Build Status](https://gitlab.com/baransu/informatycznySOK/badges/pages/build.svg)

---

Baza wiedzy kierunku: Informatyka FMI 2016-2020 PK.
Dostępna [tu](http://baransu.gitlab.io/informatycznySOK).
Pomoc w współtworzeniu mile widziana.

<!-- podac link do poradnika -->
Aby dodać zmiany stwórz nowa branch (`imie-nazwisko-cel-zmiany`), dodaj do niej zmiany, pushnij. Wtedy możesz stworzyć Merge Request i jeśli wszystkie zmiany będą ok, zostana dodane do wersji online.

* `master` posiada tylko plik `README.md` ktory zawiera konwencje oraz podstawowe informacje o projekcie.
* `pages` zawiera rzeczywista tresc strony

---

**Table of Contents**

- [Konwencja](#konwencja)
- [Baza informacji](#informacje)
- [Building locally](#building-locally)

## Konwencja

* Każdy folder (rozdział) odpowiada za jeden przedmiot, zagadnięcie, etc.
* Każdy rozdział posiada plik `README.md`, jest to plik opisujący cel i treść każdego z rozdziałów.
* Wewnątrz każdego z rodziałów możliwe jest tworzenie dowolnej liczby plikow `.md` które odpowiadają za poszczególne strony w każdym z rozdziałów.
* Lista wszystkich rozdziałów oraz stron powinna być uwzględniona w pliku `SUMMARY.md`

## Informacje
Żeby skutecznie współtworzyć projekt, trzeba ogarnać pare rzeczy:
* [markdown]() w tym piszemy rodziały, strony, etc.
* [atom]() najlepszy edytor do robienia tego ponieważ posiada miedzy innymi live preview edytowanych plików
* [git]() dzieki temu możecie pobrać projekt lokalnie a nastepnie wysłać wprowadzone zmiany
* [LaTeX]() a dokładnie notacja matematyczna, jeśli ktoś chcę pisać wzory matematyczne

## Building locally

to work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Track `pages` branch: `git checkout --track origin/pages`
3. Install GitBook `npm install gitbook-cli -g`
4. Fetch GitBook's latest stable version `gitbook fetch latest`
5. Preview your project: `gitbook serve`
6. Add content
7. Generate the website: `gitbook build` (optional)
8. Push your changes to the pages branch: `git push origin pages`

Read more at GitBook's [documentation](http://toolchain.gitbook.com/).

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

Forked from https://gitlab.com/pages/gitbook
